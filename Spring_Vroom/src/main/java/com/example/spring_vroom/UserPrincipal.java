package com.example.spring_vroom;

import java.util.Collection;
import java.util.Collections;
import com.example.spring_vroom.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserPrincipal implements UserDetails{

    private User user;

    public UserPrincipal(User user){
        super();
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("USER"));
    }

    public String getfirstName() { return user.getFirstName(); }
    public String getEmail() { return user.getEmail(); }


    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; //true for testing porpuses
    }

    @Override
    public boolean isEnabled() {
        return true; //true for testing porpuses
    }

}