package com.example.spring_vroom;

import com.example.spring_vroom.services.UserManager;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter{

    @Bean
    public AuthenticationProvider authProvider(UserManager userManager){
        var provider = new DaoAuthenticationProvider();

        provider.setUserDetailsService(userManager); //with this we can fetch data from the database
        provider.setPasswordEncoder(NoOpPasswordEncoder.getInstance()); //still need encryption

        return provider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var loginPage = "/account/login";
        http.csrf().disable() .authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .and()
                .authorizeRequests().antMatchers("/img/**","/webfonts/**", "/fonts/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/swagger-ui.html","/api/deliveries/**","/account/do_register","/account/register","/api/rider/location",loginPage).permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage(loginPage).usernameParameter("email").permitAll()
                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .and()
                .logout().invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl(loginPage).permitAll();
    }
}