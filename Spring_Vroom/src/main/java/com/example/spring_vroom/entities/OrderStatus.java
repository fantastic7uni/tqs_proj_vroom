package com.example.spring_vroom.entities;

public enum OrderStatus {
    WAITING,ACCEPTED,REFUSED,DELIVERED
}
