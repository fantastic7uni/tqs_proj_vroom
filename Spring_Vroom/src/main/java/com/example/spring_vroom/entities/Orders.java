package com.example.spring_vroom.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Orders {
    // Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User rider;

    @ManyToMany(targetEntity = Product.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private List<Product> products = new ArrayList<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private Date date;
    private String shop;
    private int shopOrderId;
    private OrderStatus orderStatus;
    private double totalPrice;
    private String address;
    private String client;
    private String clientContact;
    private double destinationLat;
    private double destinationLong;

    public Orders() {/* Constructor */ }

    public Orders(List<Product> products, Date date, String shop, int shopOrderId, OrderStatus orderStatus, double totalPrice, String address, String client, double destLat, double destLong) {
        this.products = products;
        this.date = date;
        this.shop = shop;
        this.shopOrderId = shopOrderId;
        this.orderStatus = orderStatus;
        this.totalPrice = totalPrice;
        this.address = address;
        this.client = client;
        this.destinationLat = destLat;
        this.destinationLong = destLong;
    }

    // Getters & Setters

    public double getDestinationLat() {
        return destinationLat;
    }

    public void setDestinationLat(double destinationLat) {
        this.destinationLat = destinationLat;
    }

    public double getDestinationLong() {
        return destinationLong;
    }

    public void setDestinationLong(double destinationLong) {
        this.destinationLong = destinationLong;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getShop() { return shop; }
    public void setShop(String shop) { this.shop = shop; }

    public User getRider() { return rider; }
    public void setRider(User rider) { this.rider = rider; }

    public int getShopOrderId() { return shopOrderId; }
    public void setShopOrderId(int shopOrderId) { this.shopOrderId = shopOrderId; }

    public List<Product> getProducts() { return products; }
    public void setProducts(List<Product> products) { this.products = products; }

    public double getTotalPrice() { return totalPrice; }
    public void setTotalPrice(double totalPrice) { this.totalPrice = totalPrice; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public String getClient() { return client; }
    public void setClient(String client) { this.client = client; }

    public String getClientContact() { return clientContact; }
    public void setClientContact(String clientContact) { this.clientContact = clientContact; }

    public OrderStatus getOrderStatus() { return orderStatus; }
    public void setOrderStatus(OrderStatus orderStatus) { this.orderStatus = orderStatus; }

    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", rider=" + rider +
                ", products=" + products +
                ", date=" + date +
                ", shop='" + shop + '\'' +
                ", shopOrderId=" + shopOrderId +
                ", orderStatus=" + orderStatus +
                ", total_price=" + totalPrice +
                ", address='" + address + '\'' +
                ", client='" + client + '\'' +
                ", clientContact='" + clientContact + '\'' +
                '}';
    }
}


