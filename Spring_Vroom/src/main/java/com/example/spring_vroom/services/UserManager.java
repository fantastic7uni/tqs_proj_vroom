package com.example.spring_vroom.services;

import com.example.spring_vroom.UserPrincipal;
import com.example.spring_vroom.entities.User;
import com.example.spring_vroom.exception.ResourceNotFoundException;
import com.example.spring_vroom.entities_dto.UserDTO;
import com.example.spring_vroom.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserManager implements UserDetailsService {

    @Autowired
    UserRepository userRep;

    public void registerUser(UserDTO user) throws ResourceNotFoundException{
        if (emailPhoneExist(user.getEmail(),user.getPhoneNumber())) {
            throw new ResourceNotFoundException("There is an account with that email address: "
                    + user.getEmail());
        }
        var userPersistent = new User(user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getPhoneNumber());
        userRep.save(userPersistent);
    }

    private boolean emailPhoneExist(String email, String phoneNumber) {
        return ((userRep.findByEmail(email) != null)||(userRep.findByPhoneNumber(phoneNumber) != null));
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var user = userRep.findByEmail(email);
        if(user==null)
            throw new UsernameNotFoundException("User not found");

        return new UserPrincipal(user); //return a UserPrincipal with the found user
    }

    public User getUserLoggedIn(){
        User user;
        String email = null;

        if(SecurityContextHolder.getContext().getAuthentication()!=null){

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails)
                email = ((UserPrincipal)principal).getEmail();
            if(principal instanceof String)
                email = principal.toString();

            user = userRep.findByEmail(email);
            return user;
        }
        return null;
    }
    
    public boolean updateUser(User u){
        if(userRep.existsById(u.getId())){
            userRep.save(u);
            return true;
        }
        return false;
    }

    public boolean existsById(int id){
        return userRep.existsById(id);
    }

    public User getById(int riderId) {
        return userRep.getById(riderId);
    }

    public double[] getCoordsById(Integer riderId) {
        var rider = userRep.getById(riderId);
        return new double[]{rider.getLatitude(), rider.getLongitude()};
    }
}
