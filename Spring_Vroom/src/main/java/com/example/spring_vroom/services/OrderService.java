package com.example.spring_vroom.services;

import com.example.spring_vroom.entities.OrderStatus;
import com.example.spring_vroom.entities.Orders;
import com.example.spring_vroom.entities.Product;
import com.example.spring_vroom.entities.User;
import com.example.spring_vroom.repositories.OrdersRepository;
import com.example.spring_vroom.repositories.ProductRepository;
import com.example.spring_vroom.repositories.UserRepository;
import org.apache.commons.lang3.EnumUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Collections;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    OrdersRepository orderRep;

    @Autowired
    UserRepository userRepo;

    @Autowired
    ProductRepository prodRepo;

    public Orders assign(Orders o, boolean excludeLogged){
        int id;
        User user;
        if(excludeLogged)
            id = orderRep.findRiderAssign(o.getRider().getId(),o.getDestinationLat(),o.getDestinationLong());
        else
            id = orderRep.findRiderAssign(-1, o.getDestinationLat(),o.getDestinationLong());

        if(userRepo.existsById(id)){
            user = userRepo.getById(id);
            o.setRider(user);
            for (Product p:o.getProducts())
                prodRepo.save(p);
            orderRep.save(o);
            return o;
        }
        return null;
    }

    public List<Orders> getRiderOrders(int id){
        return orderRep.findOrdersByRider(userRepo.getById(id));
    }

    public List<Orders> getAllOrdersByState(String state){
        if(EnumUtils.isValidEnum(OrderStatus.class, state))
            return orderRep.findOrdersByOrderStatus(OrderStatus.valueOf(state.toUpperCase()));
        else
            return Collections.emptyList();
    }

    public List<Orders> getAllOrders(){
        return orderRep.findAll();
    }

    public List<Orders> getRiderOrdersByState(int id, String state){
        if(EnumUtils.isValidEnum(OrderStatus.class, state))
            return orderRep.findOrdersByRiderAndOrderStatus(userRepo.getById(id),OrderStatus.valueOf(state.toUpperCase()));
        else
            return Collections.emptyList();
    }
    public Orders getRiderOrdersById(int id, int orderId){
        return orderRep.findOrdersByRiderAndId(userRepo.getById(id),orderId);
    }

    public Orders updateState(int orderId, OrderStatus state) throws JSONException {
        Orders o = orderRep.getById(orderId);
        if(!orderRep.existsById(o.getId()))
            return null;

        if (state == OrderStatus.REFUSED)
            return assign(o, true);
        else {
            if(state == OrderStatus.ACCEPTED && !getRiderOrdersByState(o.getRider().getId(),"ACCEPTED").isEmpty()) //rider ja tem uma order accepted
                return null;
            o.setOrderStatus(state);
            orderRep.save(o);
            notifyAssign(o);
            return o;
        }
    }

    private void notifyAssign(@NotNull Orders o) throws JSONException {
        var requestJSON = new JSONObject();
        requestJSON.put("id", o.getShopOrderId());
        requestJSON.put("riderId", o.getRider().getId());
        requestJSON.put("riderName",o.getRider().getFirstName());
        requestJSON.put("riderContact", o.getRider().getPhoneNumber());
        requestJSON.put("orderStatus", o.getOrderStatus());

        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(requestJSON.toString(), headers);
        //restTemplate.postForObject("http://localhost:8080/api/deliveries/assigned", request, String.class);
        restTemplate.postForObject("http://192.168.160.223:8080/api/deliveries/assigned", request, String.class);
    }
}
