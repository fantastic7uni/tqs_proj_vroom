package com.example.spring_vroom.controllers;

import com.example.spring_vroom.entities.User;
import com.example.spring_vroom.entities_dto.UserDTO;
import com.example.spring_vroom.services.OrderService;
import com.example.spring_vroom.services.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class WebController {

    @Autowired
    UserManager userManager;

    @Autowired
    OrderService orderService;

    @GetMapping("/")
    public ModelAndView home(){
        return new ModelAndView("dashboard").addObject("user",userManager.getUserLoggedIn());
    }

    @GetMapping("/account/login")
    public ModelAndView login(){
        return new ModelAndView("login");
    }

    @GetMapping("/account/register")
    public ModelAndView register(WebRequest request, Model model){
        model.addAttribute("user", new UserDTO());
        return new ModelAndView("register", (Map<String, ?>) model);
    }

    @PostMapping("/account/do_register")
    public ModelAndView processRegister(@ModelAttribute("user") UserDTO user,
                                  HttpServletRequest request,
                                  Errors errors) {
        try {
            userManager.registerUser(user);
        } catch (Exception e) {
            var mav = new ModelAndView("register");
            mav.addObject("message", "An account for that email or with that phone number already exists.");
            return mav;
        }

        return new ModelAndView("login");
    }

    @GetMapping("/account/details")
    public ModelAndView account(){
        User u = userManager.getUserLoggedIn();
        if(u == null)
            return new ModelAndView("error").addObject("msg", "No rider logged in");
        var mav = new ModelAndView("user");
        mav.addObject("user", u);
        return mav;
    }

    @GetMapping("/orders")
    public ModelAndView checkout(){
        return new ModelAndView("tables").addObject("user",userManager.getUserLoggedIn());
    }

    @GetMapping("/orders/{orderId}")
    public ModelAndView orderDetail(@PathVariable(value = "orderId") int orderId){
        var u = userManager.getUserLoggedIn();
        if(u == null)
            return new ModelAndView("/error").addObject("msg", "No rider logged in");

        var o = orderService.getRiderOrdersById(u.getId(),orderId);
        var mav = new ModelAndView("orderDetail");
        if(o == null)
            mav.addObject("msg", "Error: order details not found.");
        else
            mav.addObject("order", o);
        mav.addObject("user",u);
        return mav;
    }

    @GetMapping("/map")
    public ModelAndView map(){
        return new ModelAndView("map").addObject("user", userManager.getUserLoggedIn());
    }

    @GetMapping("/error")
    public ModelAndView error(){
        return new ModelAndView("error");
    }
}
