package com.example.spring_vroom.controllers;
import com.example.spring_vroom.entities.Orders;
import com.example.spring_vroom.entities.User;
import com.example.spring_vroom.services.OrderService;
import com.example.spring_vroom.services.UserManager;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VroomRest {

    @Autowired
    OrderService orderService;

    @Autowired
    UserManager userManager;

    @PostMapping("/deliveries/assign")
    public ResponseEntity<Object> assign(@RequestBody Orders o){
        var assignedOrder = orderService.assign(o,false);
        if(assignedOrder != null)
            return new ResponseEntity<>(assignedOrder, HttpStatus.CREATED);
        return new ResponseEntity<>("Error: failed to assign the order to a rider.",HttpStatus.NOT_FOUND);
    }

    @GetMapping("/account/deliveries")
    public ResponseEntity<Object> listRiderOrders(@RequestParam(value = "id",required = false) Integer orderId,@RequestParam(value = "state",required = false) String state){
        var loggedRider = userManager.getUserLoggedIn();
        List<Orders> ordersList;

        if(loggedRider == null)
            return new ResponseEntity<>("Error: rider must log in", HttpStatus.NOT_FOUND);

        if(orderId != null) {
            var order = orderService.getRiderOrdersById(loggedRider.getId(), orderId);
            if (order == null)
                return new ResponseEntity<>("Error: no order found for this rider with the chosen id", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(order, HttpStatus.OK);
        }

        if(state != null) {
            ordersList = orderService.getRiderOrdersByState(loggedRider.getId(), state);
            if(ordersList.isEmpty())
                return new ResponseEntity<>("No orders found with this state", HttpStatus.NOT_FOUND);
        }else{
            ordersList = orderService.getRiderOrders(loggedRider.getId());
            if(ordersList.isEmpty())
                return new ResponseEntity<>("No orders found for the user logged",HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ordersList, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/rider/location")
    public ResponseEntity<Object> getRiderLocation(@RequestParam(value = "rider") Integer riderId){
        if(!userManager.existsById(riderId))
            return new ResponseEntity<>("No rider found with that id", HttpStatus.NOT_FOUND);
        var coords = userManager.getCoordsById(riderId);
        return new ResponseEntity<>(coords, HttpStatus.OK);
    }

    @GetMapping("/deliveries")
    public ResponseEntity<Object> listAllOrders(@RequestParam(value = "state", required = false) String state){
        List<Orders> ordersList;
        if(state != null){
            ordersList = orderService.getAllOrdersByState(state);
            if(ordersList.isEmpty())
                return new ResponseEntity<>("No orders found with this state",HttpStatus.NOT_FOUND);
        }else{
            ordersList = orderService.getAllOrders();
            if(ordersList.isEmpty())
                return new ResponseEntity<>("No orders found",HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ordersList, HttpStatus.OK);
    }

    @PutMapping("/account/deliveries")
    public ResponseEntity<Object> updateState(@RequestBody Orders o) throws JSONException {
        var loggedRider = userManager.getUserLoggedIn();

        if(loggedRider != null){
            var order = orderService.updateState(o.getId(),o.getOrderStatus());
            if(order != null)
                return new ResponseEntity<>(order, HttpStatus.ACCEPTED);
            return new ResponseEntity<>("Error: it was not possible to update state", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Error:no rider logged in",HttpStatus.NOT_FOUND);
    }

    @PutMapping("/account")
    public ResponseEntity<Object> updateUser(@RequestBody User rider) {
        boolean updated = userManager.updateUser(rider);
        if(updated)
            return new ResponseEntity<>(rider, HttpStatus.OK);
        return new ResponseEntity<>("Error: failed to update rider account data.",HttpStatus.NOT_FOUND);
    }
}
