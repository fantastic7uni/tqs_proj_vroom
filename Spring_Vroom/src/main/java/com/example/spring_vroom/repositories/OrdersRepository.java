package com.example.spring_vroom.repositories;

import com.example.spring_vroom.entities.OrderStatus;
import com.example.spring_vroom.entities.Orders;
import com.example.spring_vroom.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface OrdersRepository extends JpaRepository<Orders, Integer> {
    List<Orders> findAll();
    List<Orders> findOrdersByRider(User rider);
    Orders findOrdersByRiderAndId(User rider,int orderId);
    List<Orders> findOrdersByRiderAndOrderStatus(User rider, OrderStatus os);
    List<Orders> findOrdersByOrderStatus(OrderStatus os);


    @Query(value = "SELECT id from user WHERE id != ?1 ORDER BY ABS(latitude - ?2) + ABS(longitude - ?3) LIMIT 1", nativeQuery = true)
    int findRiderAssign(int id,double latitude, double longitude);
}

