package com.example.spring_vroom.repositories;

import com.example.spring_vroom.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    List<User> findAll();
    User findByEmail(String email);
    boolean existsById(int id);
    User findByPhoneNumber(String phoneNumber);
}
