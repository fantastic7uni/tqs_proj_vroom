function updateLocation(order) {
    let latRider = order.rider.latitude
    let longRider = order.rider.longitude
    let latDestiny = order.destinationLat
    let longDestiny = order.destinationLong

    update()
    let a = setInterval(function (){
        if(Math.abs(latRider - latDestiny) > 0.3 || Math.abs(longRider - longDestiny) > 0.3)
            update();
        else
            clearInterval(a);
    }, 2000)

    function update() {
        // Update rider coords
        if (latRider < latDestiny)
            latRider+=0.2;
        else
            latRider-=0.2;

        if (longRider < longDestiny)
            longRider+=0.2;
        else
            longRider-=0.2;


        // Update New coords
        $.ajax({
            type: "PUT",
            contentType: "application/json",
            url: "api/account",
            data: JSON.stringify(
                {
                    id: order.rider.id,
                    longitude: longRider,
                    latitude: latRider,
                    email: order.rider.email,
                    phoneNumber: order.rider.phoneNumber,
                    firstName: order.rider.firstName,
                    lastName: order.rider.lastName,
                    password: order.rider.password
                }
            ),
            success: function (result) {
                console.log("Location updated: " + result.latitude+"  "+result.longitude)
            },
            error: function (e) {
                console.log("ERROR: ", e);
            }
        });
    }
}

function simulate(){
    $.ajax({
        type: "GET",
        url: "api/account/deliveries?state=ACCEPTED",
        success: function (result) {
            if(result.length > 0) {
                console.log(result)
                $.each(result,async function (i, order) {
                    console.log(order)
                    updateLocation(order)
                });
            }
        },
        error: function (e) {
            console.log("ERROR: ", e);
        }
    });
}