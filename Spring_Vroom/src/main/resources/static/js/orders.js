GET: $(document).ready(
    function() {
        document.getElementById("ordersL").style.display = 'block'
        document.getElementById("ordersA").style.display = 'none';
        ajaxGet();

        $("#filterOrderStatus").change(function (event) {
            if($("#filterOrderStatus").val() == 'ACCEPTED'){
                document.getElementById("ordersA").style.display = 'block'
                document.getElementById("ordersL").style.display = 'none'
                getAccepted();
            }
            else{
                document.getElementById("ordersL").style.display = 'block'
                document.getElementById("ordersA").style.display = 'none'
                ajaxGet();
            }
        });
        function ajaxGet() {
            $.ajax({
                type: "GET",
                url: "api/account/deliveries?state=WAITING",
                success: function (result) {
                    if (result.length == 0 && $('#listOrders p').length == 0) {
                        $('#table_body').append('<p style="color: darkred"> No pending orders</p>');
                    }
                    let trHTML = '';
                    $("#listOrders").find("tr:gt(0)").remove();
                    $.each(result,
                        function (i, order) {
                            trHTML += '<tr style="height: 75px;" ><td style = "display:none">'+ order.id +
                                '</td><td>'+ order.client +
                                '</td><td>' + order.shop +
                                '</td><td>'+ order.address +
                                '</td><td>';
                            $.each(order.products, function (i, prod){
                                trHTML += '<p>'+ prod.name +'<span>  x'+ prod.qty +'</span></p>';
                            })
                            trHTML += '</td><td style="text-align: center"><a><i style="font-size: 30px; color: darkcyan" class="fas fa-eye"></i></a></td>'+
                                '</td><td style="text-align: center"><a><i style="font-size: 30px; color: #1be611" class="fas fa-check"></i></a></td>'+
                                '</td><td style="text-align: center"><a><i style="font-size: 30px; color: red" class="fas fa-times"></i></a></td></tr>';
                        });
                    console.log("Success: ", result);
                    $('#table_body').append(trHTML);
                    ActivateOrderDetails('listOrders');
                    ActivateRefuseOrder();
                    ActivateAcceptOrder("ACCEPTED",'listOrders');

                },
                error: function (e) {
                    document.getElementById("listOrders").style.display = 'none'
                    $("#errorMsg").html("<p style='color: darkred'>Failed to Load Pending Orders</p>");
                    document.getElementById("errorMsg").style.display = 'block'
                    console.log("ERROR: ", e);
                }
            });
        }
        function getAccepted() {
            $.ajax({
                type: "GET",
                url: "api/account/deliveries?state=ACCEPTED",
                success: function (result) {
                    if (result.length == 0 && $('#listAcceptedOrders p').length == 0) {
                        $('#tableBody').html('<p style="color: darkred"> No accepted orders</p>');
                    }
                    let tbl = '';
                    $("#listAcceptedOrders").find("tr:gt(0)").remove();
                    $.each(result,
                        function (i, order) {
                            tbl += '<tr style="height: 75px;" ><td style = "display:none">'+ order.id +
                                '</td><td>'+ order.client +
                                '</td><td>' + order.shop +
                                '</td><td>'+ order.address +
                                '</td><td>';
                            $.each(order.products, function (i, prod){
                                tbl += '<p>'+ prod.name +'<span>  x'+ prod.qty +'</span></p>';
                            })
                            tbl += '</td><td style="text-align: center"><a><i style="font-size: 30px; color: darkcyan" class="fas fa-eye"></i></a></td>'+
                                '</td><td style="text-align: center"><a><i style="font-size: 30px; color: #1be611" class="fas fa-check-square"></i></a></td>';
                    });
                    console.log("Success: ", result);
                    $('#tableBody').append(tbl);
                    ActivateOrderDetails('listAcceptedOrders');
                    ActivateAcceptOrder("DELIVERED",'listAcceptedOrders');
                },
                error: function (e) {
                    document.getElementById("listAcceptedOrders").style.display = 'none'
                    $("#errorMsg").html("<p style='color: darkred'>Failed to Load Accepted Orders</p>");
                    document.getElementById("errorMsg").style.display = 'block'
                    console.log("ERROR: ", e);
                }
            });
        }
    })

function ActivateRefuseOrder() {
    let index, table = document.getElementById('listOrders');
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].cells[7].onclick = function () {
            index = this.parentElement.rowIndex;
            let deleted_id = this.parentElement.cells[0].textContent;
            $('#refuseModal').modal('show');
            $('#confirmRefuse').on('click', function() {
                UpdateOrder(deleted_id, "REFUSED");
                $('#refuseModal').modal('hide');
            });
        };
    }
}

function ActivateAcceptOrder(state,tblId) {
    let index, table = document.getElementById(tblId);
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].cells[6].onclick = function () {
            index = this.parentElement.rowIndex;
            let order_id = this.parentElement.cells[0].textContent;
            UpdateOrder(order_id, state);
        };
    }
}

function ActivateOrderDetails(tblId) {
    let table = document.getElementById(tblId);
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].cells[5].onclick = function () {
            let order_id = this.parentElement.cells[0].textContent;
            window.location = "/orders/"+order_id;
        };
    }
}

function UpdateOrder(order_id, state) {
    let formData = {
        id : order_id,
        orderStatus : state,
    }
    $.ajax({
        type: "PUT",
        contentType: "application/json",
        url: "/api/account/deliveries",
        data: JSON.stringify(formData),
        success: function (result) {
            $('#msgModal .modal-body').text("Order state updated successfully");
            $('#msgModal').modal('show');
            console.log(result);
            setTimeout(function() {location.reload()},3000)
        },
        error: function (e) {
            console.log("ERROR: ", e);
            $('#msgModal .modal-body').text("Error while updating order state. Confirm that you have no accepted order in waiting to be delivered.");
            $('#msgModal').modal('show');
            setTimeout(function() {location.reload()},4000)
        }
    });
}