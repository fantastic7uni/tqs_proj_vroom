package com.example.spring_vroom.controllers;

import com.example.spring_vroom.JsonUtils;
import com.example.spring_vroom.entities.OrderStatus;
import com.example.spring_vroom.entities.Orders;
import com.example.spring_vroom.entities.Product;
import com.example.spring_vroom.entities.User;
import com.example.spring_vroom.services.OrderService;
import com.example.spring_vroom.services.UserManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.*;
import static org.hamcrest.Matchers.hasKey;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VroomRest.class)
class RestControllerUnitTest {
    Product p1, p2, p3;
    Orders o,o1;
    User rider;
    List<Product> prods;
    List<Orders> orders;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private OrderService os;

    @MockBean
    private UserManager userManager;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        rider =  new User();
        rider.setFirstName("Tiago Tomás");
        rider.setId(1);
        rider.setLatitude(12.1);
        rider.setLongitude(-2);
        rider.setPhoneNumber("1122223456");

        p1 =  new Product("pizza",2);
        p2 =  new Product("fries",1);
        p3 =  new Product("coke",5);
        prods = new ArrayList<>();
        prods.add(p1);
        prods.add(p2);
        o = new Orders(prods, new Date(),"itadakimasu",1, OrderStatus.WAITING,30,"Aveiro","Miguel", 12.1,-5);
        o.setRider(rider);
        orders =  new ArrayList<>();
        orders.add(o);
    }

    @DisplayName("List all pending orders of a rider")
    @WithMockUser("teste@mail.pt")
    @Test
    void whenRiderHasPendingOrders_thenReturnOrders() throws Exception {
        Mockito.when(userManager.getUserLoggedIn()).thenReturn(rider);
        Mockito.when(os.getRiderOrders(Mockito.anyInt())).thenReturn(orders);

        mvc.perform(get("/api/account/deliveries"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("id")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("shop")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].client").value("Miguel"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rider.firstName").value("Tiago Tomás"));
    }

    @DisplayName("Get rider current location")
    @Test
    void whenGetRiderLocation_ReturnCoordinates() throws Exception {
        var coords = new double[]{rider.getLatitude(),rider.getLongitude()};
        Mockito.when(userManager.existsById(rider.getId())).thenReturn(true);
        Mockito.when(userManager.getCoordsById(rider.getId())).thenReturn(coords);

        mvc.perform(get("/api/rider/location?rider="+rider.getId()))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(12.1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1]").value(-2));

        verify(userManager, times(1)).getCoordsById(anyInt());
    }

    @DisplayName("List all orders in the database")
    @Test
    void whenGetAllOrders_thenReturnAllOrders() throws Exception {
        Orders o2 = new Orders(prods, new Date(),"itadakimasu",2, OrderStatus.ACCEPTED,12,"Coimbra","Daniel", 12.2,-3);
        o2.setRider(rider);
        orders.add(o2);

        Mockito.when(os.getAllOrders()).thenReturn(orders);

        mvc.perform(get("/api/deliveries"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").value(hasKey("id")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].client").value("Miguel"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].shop").value("itadakimasu"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].client").value("Daniel"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));

        verify(os, times(1)).getAllOrders();
    }

    @DisplayName("List all orders with WAITING state in the database")
    @Test
    void whenGetAllOrdersByValidState_thenReturnOrdersWithStatus() throws Exception {
        Mockito.when(os.getAllOrdersByState(any())).thenReturn(Collections.singletonList(o));

        mvc.perform(get("/api/deliveries?state="+OrderStatus.WAITING))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(JsonUtils.convertObjectToJsonString(Collections.singletonList(o))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));

        verify(os, times(1)).getAllOrdersByState(any());
    }

    @DisplayName("List all with invalid state then return not found")
    @Test
    void whenGetAllOrdersByInvalidState_thenReturn404() throws Exception {
        Mockito.when(os.getAllOrdersByState(any())).thenReturn(Collections.emptyList());

        mvc.perform(get("/api/deliveries?state=NONSENSE"))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().string("No orders found with this state"));

        verify(os, times(1)).getAllOrdersByState(any());
    }

    @DisplayName("Update rider account data")
    @WithMockUser("teste@mail.com")
    @Test
    void whenPutChangesUserAccount_ReturnSavedChanges() throws Exception {
        var newPhone = "123456789";
        rider.setPhoneNumber(newPhone);
        Mockito.when(userManager.updateUser(any())).thenReturn(true);

        mvc.perform(put("/api/account").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(rider)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(rider.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(rider.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value(newPhone));

        verify(userManager, times(1)).updateUser(any());
    }

    @DisplayName("Update order status")
    @WithMockUser("teste@mail.com")
    @Test
    void whenPutChangesOrderState_ReturnSavedChanges() throws Exception {
        Mockito.when(userManager.getUserLoggedIn()).thenReturn(rider);
        o.setOrderStatus(OrderStatus.ACCEPTED);
        Mockito.when(os.updateState(anyInt(),any())).thenReturn(o);

        mvc.perform(put("/api/account/deliveries").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(o)))
                .andExpect(status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(o.getId()))
                .andExpect(MockMvcResultMatchers.content().json(JsonUtils.convertObjectToJsonString(o)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalPrice").value(o.getTotalPrice()));

        verify(os, times(1)).updateState(anyInt(),any());
    }

    @DisplayName("Assign order to a rider and then he can see it")
    @Test
    void whenPostOrderToAssign_thenReturnAssignedToRider() throws Exception {
        o1 = new Orders(Collections.singletonList(p3), new Date(),"itadakimasu",1, OrderStatus.WAITING,30,"Sangalhos City","ines", 10.5, -2);
        o1.setRider(rider);

        when(os.assign(any(),anyBoolean())).thenReturn(o1);

        mvc.perform(post("/api/deliveries/assign").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(o1)))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.client").value("ines"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rider.firstName").value(rider.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.products[0].name").value(p3.getName()));

        verify(os, times(1)).assign(Mockito.any(),anyBoolean());
    }
}
