package com.example.spring_vroom.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import io.restassured.RestAssured;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:test.properties")
class VroomRestControllerIT {

    @LocalServerPort
    int port;

    @DisplayName("List all deliveries in bd")
    @Test
    void listDeliveries(){
        RestAssured.given().port(port).get("/api/deliveries")
            .then()
            .statusCode(200)
            .and().body("address[0]",containsString("aveiro"))
            .and().body("shop[0]",containsString("itadakimasu"))
            .and().body("address[1]",containsString("anadia"))
            .and().body("shop[1]",containsString("teste"));
    }

}
